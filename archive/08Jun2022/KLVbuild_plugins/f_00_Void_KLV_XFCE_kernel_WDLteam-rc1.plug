# f_00_Void_KLV_XFCE_kernel_WDLteam-rc1.plug
# version="1.0.0"; revision="-rc2"
# WeeDog Void outfitted with a basic commandline desktop and no kernel
# Creation date 24Sep2021; Revision date: 02Oct2021
# Copyright WeeDogLinux team; Licence MIT

# build this via terminal commands:build_firstrib_rootfs_401rc1.sh
# export CONNECTION_TIMEOUT=-1
# ./build_firstrib_rootfs_401rc1.sh void default amd64 f_00_Void_KLV_XFCE_kernel_WDLteam-rc1.plug
# Architecture i386 will probably successfully build too as an alternative to amd64

# login is user=root passwd=root

# All the parameters/commandlines can be appropriately changed:
# Simply comment in or comment out till you have what you desire
# or add new packages to the xbps-install lists.
# You can add as many valid commandlines as you want in here.
#
# base system
xbps-install -y base-minimal ncurses-base bash eudev
xbps-install -y file mc 
xbps-install -y linux linux-firmware-network wifi-firmware shadow wpa_supplicant  # needed for most wifi

# set up passwd system
pwconv
grpconv
printf "root\nroot\n" | passwd >/dev/null 2>&1 # Quietly set default root passwd to "root"

# set root to use /bin/bash
usermod --shell /bin/bash root

# Set locale to en_US.UTF-8 
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/default/libc-locales
xbps-reconfigure -f glibc-locales

# Set Bash as shell
xbps-alternatives --set bash

## --------------------------------------------------------------------------
## Xorg server, xfce4 Desktop configuration

xbps-install -y xorg xfce4 xfce4-panel xfce4-plugins compton picom
xbps-install -y gvfs 
xbps-install -y gvfs-smb gvfs-mtp gvfs-cdda

# Optional packages
#
xbps-install -y geany gftp rox ffmpeg

# Install Audio
#
xbps-install -y pulseaudio pulseaudio-utils alsa-plugins-pulseaudio alsa-utils

# Setup autologin on tty1
#
cp -a /etc/X11/xinit/xinitrc /root/.xinitrc
cp -R /etc/sv/agetty-tty1 /etc/sv/agetty-autologin-tty1
sed -i 's/GETTY_ARGS.*/GETTY_ARGS="--autologin root --noclear"/' /etc/sv/agetty-autologin-tty1/conf  # editing for autologin root

# Arrange to startx in user's .bash_profile (per Arch Wiki)
# Remove this section if not wanting boot straight into X
touch ~/.bash_profile
cat <<'AUTOLOGIN' > /etc/profile.d/autologin.sh
# autologin on tty1
if [ -z "$DISPLAY" ] && [ "$(fgconsole)" -eq 1 ]; then
#  exec startx  # remove the exec if you want back to tty1 on exit X
startx  # remove the exec if you want back to tty1 on exit X
fi
AUTOLOGIN

# Use agetty-autologin-tty1 instead of agetty-tty1 
rm -f /etc/runit/runsvdir/default/agetty-tty1
ln -s /etc/sv/agetty-autologin-tty1 /etc/runit/runsvdir/default/agetty-autologin-tty1
# enable dbus service
ln -s /etc/sv/dbus /etc/runit/runsvdir/default/dbus

# Auto-editing .xinitrc to use xfce4 instead of twm
# Because I'm using exec here the script will end there so no xterms started
sed -i 's/twm &/exec xfce4-session/' ~/.xinitrc

## USER CONFIGS: Copy main configs to /etc/skel for all normal users later added
xbps-install -y sudo
cp -af /root/. /etc/skel
mkdir -p /etc/skel/.config /etc/skel/.cache /etc/skel/.local/share
echo Still some extra to do here re the likes of runit starting pulseaudio
echo among other user needed config bits and pieces,
echo so probably a few user-config issues noted as needing fixed here

# Give wheel group nopasswd sudo rights and create weedog as wheel group member
echo '%wheel ALL=(ALL) NOPASSWD: ALL' | (VISUAL="tee -a" visudo) # wheel group added to sudo no password required
useradd -m -G wheel -s /bin/bash weedog  # weedog in wheel group so has elevated sudo permissions
printf "weedog\nweedog\n" | passwd weedog >/dev/null 2>&1 # Quietly set default weedog passwd to "weedog"

echo "desktop build process finished"
