#!/bin/bash
# make_Arch_amd64-openboxFull version 1.0.0
# Simple Build a WeeDog Arch Linux amd64 system
# Creation Date: 03May2020; Revision Date: 14June2020
# Copyright wiak (William McEwan) 03 May 2020+; Licence MIT (aka X11 license)
version="1.0.0";revision="-rc7"
progname="make_Arch_amd64-openboxFull-latest.sh"
f_plugin00="f_00_Arch_amd64-openboxFull.plug"

case "$1" in
	'--help'|'-h'|'-?') printf "./$progname [--help|--version|pause]
option 'pause' causes build to pause at start and end of root_fs
build stage\n"; exit 0;;
	'--version') printf "$progname ${version}${revision}\n"; exit 0;;
	'pause') _pause="pause";;
	*) :;;
esac

printf "Please only run this script on a Linux compatible filesystem
(e.g. ext2|3|4), from a terminal, at the build directory of your choice.
Building from an empty directory from which you wish to boot is
recommended (e.g. /mnt/sdxx/WeeDogArch/), where xx indicates partition. 

Prior to continuing, make sure you have the build-required 10 GB of free
space in the media partition directory you are installing to. Once the
build is complete you can optionally remove the very large uncompressed
firstrib_rootfs build directory, since it is no longer required if you
boot using the auto-created 01firstrib_rootfs.sfs instead.

This script will auto-download build_firstrib_rootfs-latest,
build_weedog_initrd-latest, and any required rootfs build extras (e.g.
$f_plugin00, sfs boot files, and optional utilities). It then
auto-runs some script components to completely build your WeeDog Arch
system ready for booting via your favourite grub4dos/grub2 menu.lst or 
grub.cfg, which is assumed to be already installed on your boot media.

Soon after continuing you will be asked to choose build repo. After that
the build will take a while... You might then like to go drink a coffee.
Press Enter to continue or Ctrl-C to abort without building\n" 

read  # Stop here until the Enter key is pressed or Ctrl-C to abort

# Fetch all relevant build files and utilities (take care: will not overwrite existing older ones)
wget -c https://gitlab.com/weedog/weedoglinux/-/raw/master/build_latest/build_firstrib_rootfs-latest.sh
chmod +x build_firstrib_rootfs-latest.sh
wget -c https://gitlab.com/weedog/weedoglinux/-/raw/master/build_latest/build_weedog_initrd-latest.sh
chmod +x build_weedog_initrd-latest.sh
wget -c https://gitlab.com/weedog/weedoglinux/-/raw/master/build_latest/archplugins/"${f_plugin00}"
wget -c https://gitlab.com/weedog/weedoglinux/-/raw/master/build_latest/sfs_addons/10gtkdialog64_libvte.sfs
wget -c https://gitlab.com/weedog/weedoglinux/-/raw/master/build_latest/sfs_addons/11pupdog64.sfs
wget -c https://gitlab.com/weedog/weedoglinux/-/raw/master/build_latest/utilities/modify_initrd_gz.sh
chmod +x modify_initrd_gz.sh
wget -c https://gitlab.com/weedog/weedoglinux/-/raw/master/build_latest/utilities/mount_chroot-latest.sh
chmod +x mount_chroot-latest.sh
wget -c https://gitlab.com/weedog/weedoglinux/-/raw/master/build_latest/utilities/umount_chroot-latest.sh
chmod +x umount_chroot-latest.sh

case "$_pause" in
	'pause') printf "\n##### All default build components are now ready.
#### If there is anything you wish to manually add or change, do it now.
#### Press Enter key when ready for stage2: build rootfs\n"; read;;
	*) :;;
esac

# Build WeeDog!
./build_firstrib_rootfs-latest.sh arch default amd64 "${f_plugin00}"
case "$_pause" in
	'pause') printf "\nPress Enter key when ready for stage3: build initrd\n"; read;;
	*) :;;
esac
./build_weedog_initrd-latest.sh arch  # add extra options here if desired

exit 0
