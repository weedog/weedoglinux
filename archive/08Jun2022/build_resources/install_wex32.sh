xbps-install -Sy giblib  # you don't need mrxvt if you already have xterm, uxterm, rxvt, urxvt, or aterm
mkdir -p /etc/alsa/conf.d
ln -s /usr/share/alsa/alsa.conf.d/50-pulseaudio.conf /etc/alsa/conf.d/50-pulseaudio.conf  # pulseaudio 'pulse' DEVICE name fix
cd / && wget -c https://gitlab.com/weedog/weedoglinux/-/raw/master/build_resources/wex_scrox32_gifenc_precord.tar.xz -O /wex.tar.xz
tar xJf /wex.tar.xz && rm /wex.tar.xz
