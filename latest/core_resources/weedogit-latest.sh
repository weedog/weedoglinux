#!/bin/sh
# Creation Date: 14feb2022; Revision Date: 09Aug2022
# Copyright wiak (William McEwan) 14feb2022+; Licence MIT (aka X11 license)
# Run this script from directory you want to frugal boot from.
# You can pre-download the iso if you wish into same directory, or
# let weedogit.sh fetch it for you.
# NOTE WELL: If pre-downloading iso it must be stored with exactly same filename as download URLnn filename below
progname="weedogit.sh"; version="015"; revision="-rc1"
# Added: linux-lite, bunsen-lithium LXLE-Focal,
# tails-amd64 (part working only - ctrl-alt f2 to commandline login root:root; reboot -f to restart),
# Debian-live, Regata_OS (password is blank, can: sudo password root to user:pw root:root),
# Kodachi, SysLinuxOS (for passwords check distro websites or try user:pw weedog:weedog or root:root)

# Current distro URL addresses.
# NOTE WELL: User of this script will NEED to check and change the detail(s) as and
# when upstream iso version and thus URL changes (which often happens a lot!)
URL01="https://github.com/vanilla-dpup/releases/releases/download/vanilladpup-x86_64-9.2.9/vanilladpup-9.2.9-extra.tar" #No time to test - may have changed approach
# previous: "https://github.com/vanilla-dpup/releases/releases/download/vanilladpup-x86_64-9.1.24/vanilladpup-9.1.24.tar"
URL02="https://iso.pop-os.org/22.04/amd64/intel/11/pop-os_22.04_amd64_intel_11.iso"
URL03="https://github.com/endeavouros-team/ISO/releases/download/1-EndeavourOS-ISO-releases-archive/EndeavourOS_Artemis-22_6.iso"
URL04="https://download.manjaro.org/xfce/21.3.6/manjaro-xfce-21.3.6-minimal-220729-linux515.iso"
URL05="https://download.manjaro.org/kde/21.3.6/manjaro-kde-21.3.6-minimal-220729-linux515.iso"
URL06="https://download.manjaro.org/gnome/21.3.6/manjaro-gnome-21.3.6-minimal-220729-linux515.iso"
URL07="https://gigenet.dl.sourceforge.net/project/makulu/downloads/Shift/MakuluLinux-Shift-U-2022-07-18.iso"
URL08="https://sourceforge.net/projects/mx-linux/files/Final/Xfce/MX-21.1_x64.iso"
URL09="https://sourceforge.net/projects/mx-linux/files/Final/Xfce/MX-21.1_ahs_x64.iso"
URL10="https://sourceforge.net/projects/mx-linux/files/Final/KDE/MX-21.1_KDE_x64.iso"
URL11="https://sourceforge.net/projects/mx-linux/files/Final/Fluxbox/MX-21.1_fluxbox_x64.iso"
URL12="https://sourceforge.net/projects/mx-linux/files/Final/Fluxbox/MX-21.1_fluxbox_386.iso"
URL13="https://mirror.freedif.org/zorin/16/Zorin-OS-16.1-Lite-64-bit.iso"
URL14="https://files.kde.org/neon/images/user/20220804-0950/neon-user-20220804-0950.iso"
URL15="https://sourceforge.net/projects/garuda-linux/files/garuda/sway/220717/garuda-sway-linux-zen-220717.iso"
URL16="https://sourceforge.net/projects/bodhilinux/files/6.0.0/bodhi-6.0.0-64.iso"
URL17="https://github.com/MassOS-Linux/MassOS/releases/download/v2022.08/massos-2022.08-x86_64-xfce.iso"
URL18="http://repo.tinycorelinux.net/13.x/x86_64/release/TinyCorePure64-current.iso"
URL19="https://rockedge.org/kernels/data/ISO/Kennel_Linux/Airedale/beta16/KLV-Airedale-beta16.iso"
URL20="https://cdn.download.clearlinux.org/releases/36010/clear/clear-36010-live-desktop.iso"
URL21="https://mirror.fsmg.org.nz/ubuntu-releases/22.04/ubuntu-22.04-desktop-amd64.iso"
URL22="https://mirrors.layeronline.com/linuxmint/stable/21/linuxmint-21-cinnamon-64bit.iso"
URL23="https://osdn.net/dl/linuxlite/linux-lite-6.0-64bit.iso"
URL24="https://ddl.bunsenlabs.org/ddl/lithium-3-amd64.hybrid.iso"
URL25="https://sourceforge.net/projects/lxle/files/Final/OS/Focal-64/LXLE-Focal-Release.iso"
URL26="http://dl.amnesia.boum.org/tails/stable/tails-amd64-5.3.1/tails-amd64-5.3.1.iso"
URL27="https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/debian-live-11.4.0-amd64-xfce.iso"
# URL27="https://cdimage.debian.org/images/unofficial/non-free/images-including-firmware/current-live/amd64/iso-hybrid/debian-live-11.4.0-amd64-xfce+nonfree.iso"
URL28="https://osdn.net/dl/regataos/Regata_OS_22_en-US.x86_64-22.0.3.iso"
# URL28="https://pt.osdn.net/dl/regataos/Regata_OS_22-nv_en-US.x86_64-22.0.3.iso" # this alternative for modern NVIDIA support
URL29="https://sourceforge.net/projects/linuxkodachi/files/kodachi-8.24-64.iso"
URL30="https://sourceforge.net/projects/syslinuxos/files/SysLinuxOS-amd64_2022-07-26_1858.iso"
#URL31="https://gigenet.dl.sourceforge.net/project/linux4m/40.0/livecd/4MLinux-40.0-64bit.iso"
URL31="https://sourceforge.net/projects/linux4m/files/40.0/livecd/4MLinux-40.0-64bit.iso"

# Default frugal install subdirectories
# User of this script can change the names if they wish)
# For example, distro04="ManjaroXFCE"; subdirectory04="ManjaX"
distro01="vanilladpup64";                   subdirectory01="KL_vdpup64"
distro02="PopOS64";                         subdirectory02="WDL_pop-os" 
distro03="EndeavourOS XFCE";                subdirectory03="WDL_endeavXFCE"
distro04="manjaro-xfce";                    subdirectory04="WDL_manjaXFCE"
distro05="manjaro-kde";                     subdirectory05="WDL_manjaKDE"
distro06="manjaro-gnome";                   subdirectory06="WDL_manjaGNOME"
distro07="MakuluLinux-Shift-x64-Ubuntu";    subdirectory07="WDL_makuluU"
distro08="MX-21_X64 XFCE";                  subdirectory08="WDL_MXxfce"
distro09="MX-21_X64 XFCE ahs";              subdirectory09="WDL_MXxfce_ahs" # advanced hardware support - newer PCs
distro10="MX-21_X64 KDE";                   subdirectory10="WDL_MXkde"      # with ahs for newer PCs 
distro11="MX-21_X64 fluxbox";               subdirectory11="WDL_MxFlux"
distro12="MX-21_386 fluxbox";               subdirectory12="WDL_MxFlux32"   # 32bit variant
distro13="Zorin Lite XFCE";                 subdirectory13="WDL_zorinlXFCE"
distro14="NeonKDE";                         subdirectory14="WDL_neonKDE"
distro15="GarudaSway";                      subdirectory15="WDL_garuSWAY"
distro16="Bodhi Std";                       subdirectory16="WDL_bod"
distro17="MassOS";                          subdirectory17="WDL_masso"
distro18="TinyCoreLinux64 flwm";            subdirectory18="KL_TCL64"
distro19="KLV-Airedale64";                  subdirectory19="KLV-Airedale64" # frugal install from official iso
distro20="Intel Clear Linux";               subdirectory20="WDL_intclear"
distro21="Ubuntu_live";                     subdirectory21="WDL_buntu"
distro22="Mint Cinnamon live";              subdirectory22="WDL_mintCin"
# --- NEW ---
distro23="linux-lite";                      subdirectory23="WDL_linlite"
distro24="bunsen-lithium";                  subdirectory24="WDL_bunsenlithium"
distro25="LXLE-Focal";                      subdirectory25="WDL_LXLE-Focal"
distro26="tails-amd64";                     subdirectory26="WDL_tails-amd64"
distro27="Debian_live";                     subdirectory27="WDL_debi"
distro28="Regata_OS";                       subdirectory28="WDL_regata"
distro29="Kodachi";                         subdirectory29="WDL_kodachiXFCE"
distro30="SysLinuxOS";                      subdirectory30="WDL_syslinuxMATE"
distro31="4MLinux";                         subdirectory31="WDL_4ML"       # extremely experimental WDL conversion!
# note: users are invited to add additional distros above!

#Initialise
mkdir -p /tmp/weedogit/miso /tmp/weedogit/miso2 /tmp/weedogit/fsroot /tmp/weedogit/merged upper_changes work

_release (){
			if [ $1 ];then
			 release="$1"
			else
			 printf "Press Enter to use script's URL for the iso
OR change script to use valid current URL, exit with ctrl-c and re-run: "
			 read release
			 [ -z $release ] && release="$default_release"
			fi
}

_get_WDL_initrd (){
  wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/HRZhsnouSm3Gpf3/download -O modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh
  wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/CXixDo8Dd2rtxSQ/download -O initrd.gz
  wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/8aFjxy5QrjfyI8o/download -O w_init
}
_get_WDL_initrd_kmod (){ # kmod needed just now for zstd compressed modules
  wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/HRZhsnouSm3Gpf3/download -O modify_initrd_gz.sh && chmod +x modify_initrd_gz.sh
  wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/yIHevOcDCZf1xzX/download -O initrd.gz
  wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/8aFjxy5QrjfyI8o/download -O w_init
}

_set_passwords (){ # for root user and normal user weedog
  # So here is how to make a temporary overlay of ro X.sfs and rw upper_changes
  sudo mount -t overlay -o \
lowerdir=/tmp/weedogit/fsroot,\
upperdir=upper_changes,\
workdir=work \
overlay_result /tmp/weedogit/merged
  # simple chroot but not mounting /proc /dev and so on here - might need in more complex additions followed by umount -l. Following may need 'tweaked' but seems ok
  cat << INSIDE_CHROOT | LC_ALL=C chroot /tmp/weedogit/merged sh
export PATH=/sbin:/usr/sbin:/bin:/usr/bin
printf "root\nroot" | passwd root >/dev/null 2>&1 # Quietly set default root passwd to "root"
useradd -m -s /bin/bash weedog # create user weedog with home dir and bash sh
printf "weedog\nweedog" | passwd weedog >/dev/null 2>&1 # Quietly set default weedog passwd to "weedog"
usermod -G sudo weedog # adds weedog to group sudo if it exists
usermod -G wheel weedog # adds weedog to group wheel if it exists
exit
INSIDE_CHROOT
}

_grub_config (){
	printf "
You should now create appropriate grub.conf or menu.lst stanza
Examples follow. Substitute in your own uuid and bootdir_name details
Check the actual names for files vmlinuz and initrd.gz in bootdir_name

grub.cfg:
menuentry \"${subdir}\" {
  insmod ext2
  search --no-floppy --fs-uuid --set 9f8b9e81-9c04-41ca-ace0-d37b1787a94d
  linux /$subdir/vmlinuz w_bootfrom=UUID=9f8b9e81-9c04-41ca-ace0-d37b1787a94d=/$subdir
  initrd /$subdir/initrd.gz
}
menu.lst:
title $subdir
  find --set-root uuid () 9f8b9e81-9c04-41ca-ace0-d37b1787a94d
  kernel /$subdir/vmlinuz w_bootfrom=UUID=9f8b9e81-9c04-41ca-ace0-d37b1787a94d=/$subdir
  initrd /$subdir/initrd.gz
  
Note: for vanilladpup you need extra kernel linux argument: fwmod=usrlib

Once grub configured you should be able to boot the new install

Refer to $PWD/grub_config.txt for
copy of this information plus blkid info\n" | tee "${PWD}"/grub_config.txt
	blkid >> "${PWD}"/grub_config.txt
}

# Following under test - only using for EndeavourOS and Pop!_OS at the moment
_extractiso () { # 14Apr2022 provided by Puppy forum member Keef, thanks!
    echo "Extracting large files from iso. Please wait patiently ..."
    mount "$isoname" /tmp/weedogit/miso
    cp -a $(find /tmp/weedogit/miso -name "vmlinuz*" | head -n 1) vmlinuz
    sqfs=$(find /tmp/weedogit/miso -type f -exec file {} \; | grep "Squashfs" | cut -d":" -f1)
    cp -a "$sqfs" 08filesystem.sfs
    sync;sync
    mount 08filesystem.sfs /tmp/weedogit/fsroot
}

PWD=`pwd`

if [ $1 ];then
  distro="$1"
else
  printf "
Make a WeeDogged frugal install using WDL skeleton initrd
Choose a distro to WeeDogIt

1 $distro01 (weedogged - using WDL initrd, not frugalify)
2 $distro02
3 $distro03
4 $distro04
5 $distro05
6 $distro06
7 $distro07
8 $distro08
9 $distro09
10 $distro10
11 $distro11
12 $distro12
13 $distro13
14 $distro14
15 $distro15 (warning: Cancel out of disk partitioning install)
16 $distro16
17 $distro17
18 $distro18
19 $distro19
20 $distro20
21 $distro21
22 $distro22
23 $distro23
24 $distro24
25 $distro25
26 $distro26
27 $distro27
28 $distro28
29 $distro29
30 $distro30
31 $distro31
q quit (more choices may come in later release)
Enter distro choice (i.e. 1, 2, 3 ...etc or q for quit): "
# note: users are invited to add additional distros above!
  read distro
fi

case "$distro" in
  '--version') printf "$progname ${version}${revision}\n"; exit 0;;
  ''|'-h'|'--help'|'-?') 
		printf "$progname ${version}${revision}\n"
		printf "To make a WeeDogged frugal install using WDL skeleton initrd

Simply execute this script with command:

./$progname\n"; exit 0;;
  1)	distro="vanilladpup"
		subdir="$subdirectory01"
		arch="x86_64"
		default_release="9.2.9"
		_release "$2"
		echo "Fetching and processing large files. Please wait patiently ..."
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL01}"
		tar xvf ${distro}-${release}.tar
		_get_WDL_initrd
		wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/SxsBbQy2cYeGGfq/download -O WDLfork_vdpup.tar
		tar xvf WDLfork_vdpup.tar
		sync;sync
		mv zdrv_${distro}_${release}.sfs 00zdrv_${distro}_${release}.sfs
		mv fdrv_${distro}_${release}.sfs 01fdrv_${distro}_${release}.sfs
		mv bdrv_${distro}_${release}.sfs 02bdrv_${distro}_${release}.sfs
		mv puppy_${distro}_${release}.sfs 08puppy_${distro}_${release}.sfs
		mv adrv_${distro}_${release}.sfs 20adrv_${distro}_${release}.sfs
		mv devx_${distro}_${release}.sfs 21devx_${distro}_${release}.sfs
		mv docx_${distro}_${release}.sfs 22docx_${distro}_${release}.sfs
		mv nlsx_${distro}_${release}.sfs 23nlsx_${distro}_${release}.sfs
		;;
  2)	distro="pop-os"
		subdir="$subdirectory02"
		arch="amd64_intel_7"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL02}" 
		sync;sync
		isoname="${URL02##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  3)	distro="EndeavourOS_Atlantis_neo"
		subdir="$subdirectory03"
		arch="x86_64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL03}"
#		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		isoname="${URL03##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
#		mount "${URL03##*/}" /tmp/weedogit/miso
#		cp -a /tmp/weedogit/miso/arch/x86_64/airootfs.sfs 08airootfs.sfs
#		sync;sync
#		mount 08airootfs.sfs /tmp/weedogit/fsroot
#		cp -a /tmp/weedogit/fsroot/usr/lib/modules/5.17.1-arch1-1/vmlinuz .
		_get_WDL_initrd_kmod
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  4)	distro="manjaro-xfce"
		subdir="$subdirectory04"
		arch="x86_64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL04}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL04##*/}" /tmp/weedogit/miso
		cp -a /tmp/weedogit/miso/manjaro/x86_64/rootfs.sfs 04rootfs.sfs
		cp -a /tmp/weedogit/miso/manjaro/x86_64/desktopfs.sfs 05desktopfs.sfs
		cp -a /tmp/weedogit/miso/manjaro/x86_64/mhwdfs.sfs 06mhwdfs.sfs
		cp -a /tmp/weedogit/miso/manjaro/x86_64/livefs.sfs 07livefs.sfs
		sync;sync
		mount 04rootfs.sfs /tmp/weedogit/fsroot
		cp -a /tmp/weedogit/fsroot/boot/vmlinuz* vmlinuz
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  5)	distro="manjaro-kde"
		subdir="$subdirectory05"
		arch="x86_64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL05}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL05##*/}" /tmp/weedogit/miso
		cp -a /tmp/weedogit/miso/manjaro/x86_64/rootfs.sfs 04rootfs.sfs
		cp -a /tmp/weedogit/miso/manjaro/x86_64/desktopfs.sfs 05desktopfs.sfs
		cp -a /tmp/weedogit/miso/manjaro/x86_64/mhwdfs.sfs 06mhwdfs.sfs
		cp -a /tmp/weedogit/miso/manjaro/x86_64/livefs.sfs 07livefs.sfs
		sync;sync
		mount 04rootfs.sfs /tmp/weedogit/fsroot
		cp -a /tmp/weedogit/fsroot/boot/vmlinuz* vmlinuz
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  6)	distro="manjaro-gnome"
		subdir="$subdirectory06"
		arch="x86_64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL06}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL06##*/}" /tmp/weedogit/miso
		cp -a /tmp/weedogit/miso/manjaro/x86_64/rootfs.sfs 04rootfs.sfs
		cp -a /tmp/weedogit/miso/manjaro/x86_64/desktopfs.sfs 05desktopfs.sfs
		cp -a /tmp/weedogit/miso/manjaro/x86_64/mhwdfs.sfs 06mhwdfs.sfs
		cp -a /tmp/weedogit/miso/manjaro/x86_64/livefs.sfs 07livefs.sfs
		sync;sync
		mount 04rootfs.sfs /tmp/weedogit/fsroot
		cp -a /tmp/weedogit/fsroot/boot/vmlinuz* vmlinuz
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  7)	distro="MakuluLinux-Shift-x64-U"
		subdir="$subdirectory07"
		arch="x86_64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL07}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL07##*/}" /tmp/weedogit/miso
		cp -a /tmp/weedogit/miso/live/filesystem.squashfs 08filesystem.sfs
		sync;sync
		mount 08filesystem.sfs /tmp/weedogit/fsroot
		cp -a /tmp/weedogit/fsroot/boot/vmlinuz* vmlinuz
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  8)	distro="MX" # XFCE stable for PC a few years old
		subdir="$subdirectory08"
		arch="x64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL08}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL08##*/}" /tmp/weedogit/miso
		cp -a /tmp/weedogit/miso/antiX/linuxfs 08linuxfs.sfs
		sync;sync
		mount 08linuxfs.sfs /tmp/weedogit/fsroot
		cp -a /tmp/weedogit/fsroot/boot/vmlinuz* vmlinuz
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  9)	distro="MX" # XFCE advanced hardware support (ahs) for newer machines
		subdir="$subdirectory09"
		arch="x64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL09}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL09##*/}" /tmp/weedogit/miso
		cp -a /tmp/weedogit/miso/antiX/linuxfs 08linuxfs.sfs
		sync;sync
		mount 08linuxfs.sfs /tmp/weedogit/fsroot
		cp -a /tmp/weedogit/fsroot/boot/vmlinuz* vmlinuz
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  10)	distro="MX" # KDE with advanced hardware support (ahs) for newer machines
		subdir="$subdirectory10"
		arch="x64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL10}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL10##*/}" /tmp/weedogit/miso
		cp -a /tmp/weedogit/miso/antiX/linuxfs 08linuxfs.sfs
		sync;sync
		mount 08linuxfs.sfs /tmp/weedogit/fsroot
		cp -a /tmp/weedogit/fsroot/boot/vmlinuz* vmlinuz
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  11)	distro="MX" # fluxbox with advanced hardware support (ahs) for newer machines
		subdir="$subdirectory11"
		arch="x64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL11}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL11##*/}" /tmp/weedogit/miso
		cp -a /tmp/weedogit/miso/antiX/linuxfs 08linuxfs.sfs
		sync;sync
		mount 08linuxfs.sfs /tmp/weedogit/fsroot
		cp -a /tmp/weedogit/fsroot/boot/vmlinuz* vmlinuz
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  12)	distro="MX" # fluxbox with advanced hardware support (ahs) for newer machines
		subdir="$subdirectory12"
		arch="386"  # yeah!  A 32bit version of the OS...
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL12}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL12##*/}" /tmp/weedogit/miso
		cp -a /tmp/weedogit/miso/antiX/linuxfs 08linuxfs.sfs
		sync;sync
		mount 08linuxfs.sfs /tmp/weedogit/fsroot
		cp -a /tmp/weedogit/fsroot/boot/vmlinuz* vmlinuz
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  13)	distro="Zorin-OS"
		subdir="$subdirectory13"
		arch="Lite-64-bit"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL13}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL13##*/}" /tmp/weedogit/miso
		cp -a /tmp/weedogit/miso/casper/filesystem.squashfs 08filesystem.sfs
		sync;sync
		mount 08filesystem.sfs /tmp/weedogit/fsroot
		cp -a /tmp/weedogit/fsroot/boot/vmlinuz-* vmlinuz
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  14)	distro="neon-user"
		subdir="$subdirectory14"
		arch="x86_64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL14}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL14##*/}" /tmp/weedogit/miso
		cp -a /tmp/weedogit/miso/casper/filesystem.squashfs 08filesystem.sfs
		sync;sync
		mount 08filesystem.sfs /tmp/weedogit/fsroot
		cp -a /tmp/weedogit/miso/casper/vmlinuz vmlinuz
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  15)	distro="garuda"
		subdir="$subdirectory15"
		arch="sway"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL15}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL15##*/}" /tmp/weedogit/miso
		cp -a /tmp/weedogit/miso/${distro}/x86_64/rootfs.sfs 04rootfs.sfs
		cp -a /tmp/weedogit/miso/${distro}/x86_64/desktopfs.sfs 05desktopfs.sfs
		cp -a /tmp/weedogit/miso/${distro}/x86_64/mhwdfs.sfs 06mhwdfs.sfs
		cp -a /tmp/weedogit/miso/${distro}/x86_64/livefs.sfs 07livefs.sfs
		sync;sync
		mount 04rootfs.sfs /tmp/weedogit/fsroot
		cp -a /tmp/weedogit/fsroot/boot/vmlinuz* vmlinuz
		_get_WDL_initrd_kmod
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  16)	distro="bodhi"
		subdir="$subdirectory16"
		arch="64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL16}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL16##*/}" /tmp/weedogit/miso
		cp -a /tmp/weedogit/miso/casper/filesystem.squashfs 08filesystem.sfs
		sync;sync
		mount 08filesystem.sfs /tmp/weedogit/fsroot
		cp -a /tmp/weedogit/miso/casper/vmlinuz vmlinuz
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  17)	distro="massos"
		subdir="$subdirectory17"
		arch="x86_64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL17}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL17##*/}" /tmp/weedogit/miso
		# horrible hack, but works to mount squashfs.img as 08filesystem.sfs later:
		cp -a /tmp/weedogit/miso/LiveOS/squashfs.img 08filesystem.sfs
		sync;sync
		mount 08filesystem.sfs /tmp/weedogit/fsroot
		cp -a /tmp/weedogit/miso/casper/vmlinuz vmlinuz
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  18)	distro="TinyCorePure64-current"LXLE-Focal 
		subdir="$subdirectory18"
		arch="x86_64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# tcz repos via: MIRROR="${MIRROR%/}/$(getMajorVer).x/$BUILD/tcz"
		# where MIRROR=repo.tinycorelinux.net and BUILD=x86_64
		# e.g. https://repo.tinycorelinux.net/13.x/x86_64/tcz/file.tcz
		# and check dependencies in file file.tcz.dep
		# or fetch via: https://ftp.nluug.nl/os/Linux/distr/tinycorelinux/
		
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL18}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL18##*/}" /tmp/weedogit/miso
		cp -a /tmp/weedogit/miso/cde/optional/* .
		rm -f *.md5.txt
		c=11
		for f in *.tcz; do  # rename required tcz apps to numbered sfs form
		  mv "$f" "${c}${f}.sfs"
		  c=$((c+1))
		done
		mkdir -p 08core # for core TCL root filesystem (uncompressed)
		cp -a /tmp/weedogit/miso/boot/corepure64.gz 08core/
		cd 08core && zcat corepure64.gz | cpio -idm
		sync;sync
		# sed -i.bak '/USER="tc"/iNOZSWAP=1' etc/init.d/tc-config
		# now done by /usr/local/tc_installed.sh as is echo Xfbdev > etc/sysconfig/Xserver
		rm -f corepure64.gz
		cd ..
		_get_WDL_initrd
		wget -c --no-check-certificate https://owncloud.rockedge.org/index.php/s/gK3wm2Z7QdoqHe7/download -O WDLfork_TCL.tar
		tar xvf WDLfork_TCL.tar
		sync;sync
		# fetch KLV-Airedale kernel components (TCL kernel has no overlayfs)
		echo "Fetching huge kernel/modules/firmware. Please wait patiently ..."
		mkdir -p kernelmodules
		cd kernelmodules
		wget -c --no-check-certificate https://rockedge.org/kernels/data/Kernels/64bit/5.14.1_untested/huge-5.14.1-bionicpup64.tar.bz2
		tar xjvf huge-5.14.1-bionicpup64.tar.bz2
		mv vmlinuz-5.14.1-bionicpup64 ../vmlinuz
		mv kernel-modules-5.14.1-bionicpup64.sfs ../00kernel-modules-5.14.1-bionicpup64.sfs
		mv fdrv-5.14.1-bionicpup64.sfs ../01fdrv-5.14.1-bionicpup64.sfs
		rm -f WDLfork_vdpup.tar
		sync;sync
		cd ..
		;;
  19)	distro="Airedale"
		subdir="$subdirectory19"
		arch="x86_64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL19}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL19##*/}" /tmp/weedogit/miso
		cp -a /tmp/weedogit/miso/* .
		;;
		# note: users are invited to add additional distros above!
  20)	distro="clearlinux"
		subdir="$subdirectory20"
		arch="x86_64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL20}"
		echo "Extracting large files from iso. Please wait patiently ..."
		sync;sync
		mount "${URL20##*/}" /tmp/weedogit/miso
		# horrible hack, but works to mount raw img "rootfs.img" as 08filesystem.sfs later:
		cp -a /tmp/weedogit/miso/images/rootfs.img 08filesystem.sfs
		sync;sync
		mount 08filesystem.sfs /tmp/weedogit/fsroot
		cp -a /tmp/weedogit/miso/kernel/kernel.xz vmlinuz
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords
		;;
  21)	distro="Ubuntu"
		subdir="$subdirectory21"
		arch="amd64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL21}" 
		sync;sync
		isoname="${URL21##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  22)	distro="MintCin"
		subdir="$subdirectory22"
		arch="amd64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL22}" 
		sync;sync
		isoname="${URL22##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  23)	distro="linux-lite"
		subdir="$subdirectory23"
		arch="amd64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL23}" 
		sync;sync
		isoname="${URL23##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  24)	distro="bunsen-lithium"
		subdir="$subdirectory24"
		arch="amd64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL24}" 
		sync;sync
		isoname="${URL24##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  25)	distro="LXLE-Focal"
		subdir="$subdirectory25"
		arch="amd64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL25}" 
		sync;sync
		isoname="${URL25##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  26)	distro="tails-amd64"
		subdir="$subdirectory26"
		arch="amd64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL26}" 
		sync;sync
		isoname="${URL26##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  27)	distro="Debian"
		subdir="$subdirectory27"
		arch="amd64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL27}" 
		sync;sync
		isoname="${URL27##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  28)	distro="Regata_OS"
		subdir="$subdirectory28"
		arch="amd64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL28}" 
		sync;sync
		isoname="${URL28##*/}"
		# Needing extra img decompress so cannot use this here: _extractiso
		echo "Extracting large files from iso. Please wait patiently ..."
		mount "$isoname" /tmp/weedogit/miso
		sleep 1
		mount /tmp/weedogit/miso/LiveOS/squashfs.img /tmp/weedogit/miso2
		sleep 1
		mount /tmp/weedogit/miso2/LiveOS/rootfs.img /tmp/weedogit/fsroot		
		sleep 1
		cp -a $(find /tmp/weedogit/fsroot -name "vmlinuz*default") vmlinuz
		mksquashfs /tmp/weedogit/fsroot/ 08filesystem.sfs
		_get_WDL_initrd_kmod
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords (password is blank, can: sudo password root to user:pw root:root)
		;;
  29)	distro="Kodachi"
		subdir="$subdirectory29"
		arch="amd64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL29}" 
		sync;sync
		isoname="${URL29##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  30)	distro="SysLinuxOS"
		subdir="$subdirectory30"
		arch="amd64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL30}" 
		sync;sync
		isoname="${URL30##*/}"
		_extractiso  # extracts sfs and vmlinuz from iso
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		sync;sync
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a /tmp/weedogit/fsroot/usr/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
		_set_passwords
		;;
  31)	distro="4MLinux"
		subdir="$subdirectory31"
		arch="amd64"
		default_release="Press Enter to accept default or check and change"
		_release "$2"
		# CHECK online for the current iso url and filename prior to use
		# MODIFY the iso url and filename details at beginning of this script when necessary
		wget -c --no-check-certificate "${URL31}" 
		sync;sync
		isoname="${URL31##*/}"
		# Needing extra img decompress so cannot use this here: _extractiso
		echo "Extracting large files from iso. Please wait patiently ..."
		mount "$isoname" /tmp/weedogit/miso
		sleep 1
		mkdir -p 08initrd_decomp
		cd 08initrd_decomp
		zcat /tmp/weedogit/miso/boot/initrd.gz | cpio -idm
		tar xJvf /tmp/weedogit/miso/drivers/addon_modules-all*
		tar xJvf /tmp/weedogit/miso/drivers/addon_firmware*
		k=`ls lib/modules`
		depmod -a $k -b .
		cd ..
		mkdir -p initrd2_decomp
		cd initrd2_decomp
		zcat /tmp/weedogit/miso/boot/initrd2.gz | cpio -idm
		cd ..
		cp -a /tmp/weedogit/miso/boot/bzImage vmlinuz		
		_get_WDL_initrd
		mv initrd.gz initrdSKEL.gz
		mkdir -p initrd_decompressed
		cd initrd_decompressed
		zcat ../initrdSKEL.gz | cpio -idm
		cp -a ../08initrd_decomp/lib/modules/* usr/lib/modules/		
		echo "Result being compressed. Please wait patiently ..."
		sync;sync
		find . | cpio -oH newc 2>/dev/null | gzip > ../initrd.gz 
		cd ..
#		_set_passwords (password is blank, can: sudo password root to user:pw root:root)
		;;
		*) printf "$progname ${version}${revision}\n"; exit 0;;
esac

# clean up
sync;sync
umount -l /tmp/weedogit/merged
umount -l /tmp/weedogit/fsroot
umount -l /tmp/weedogit/miso
umount -l /tmp/weedogit/miso2
rm -rf /tmp/weedogit/
rm -rf initrd_decompressed

_grub_config
exit 0
