#!/bin/sh
# Creation Date: 20Jun2020; Revision Date: 04Sep2021
# Copyright wiak (William McEwan) 20Jun2020+; Licence MIT (aka X11 license)
progname="get_WDL_10gtkdialogGTK3_filemnt64_sfs"; version="4.0.0"; revision="-rc1"
resource=${progname#get_}; resource=${resource%.sh}

case "$1" in
	'--help'|'-h'|'-?') printf "Simply execute this script with command:
./${progname}
to download $resource
to the current directory\n"; exit 0;;
	'--version') printf "$progname ${version}${revision}\n"; exit 0;;
esac

printf "\nPress Enter to download $resource\nor press Ctrl-C to abort download\n" 
read  # Stop here until the Enter key is pressed or Ctrl-C to abort
wget -c http://owncloud.rockedge.org/index.php/s/WG4Xu6a1OblHg4v/download -O 10gtkdialogGTK3_filemnt64.sfs

exit 0
